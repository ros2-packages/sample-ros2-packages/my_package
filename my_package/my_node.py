import rclpy
from rclpy.node import Node

class GetParams(Node):
    def __init__(self):
        super().__init__('node_standalone')
        self.declare_parameter('my_parameter')
        self.declare_parameter('greet_param')
        param_str = self.get_parameter('my_parameter')
        self.get_logger().info('Hello %s!!!' % str(param_str.value))
        timer_period = 1  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def timer_callback(self):
        # First get the value parameter "my_parameter" and get its string value
        my_param = self.get_parameter("greet_param").get_parameter_value().string_value

        # Send back a hello with the name
        self.get_logger().info('Hello %s!' % my_param)

        # Then set the parameter "my_parameter" back to string value "world"
        # my_new_param = rclpy.parameter.Parameter(
        #     "greet_param",
        #     rclpy.Parameter.Type.STRING,
        #     "world"
        # )
        # all_new_parameters = [my_new_param]
        # self.set_parameters(all_new_parameters)


# The following is just to start the node
def main(args=None):
    rclpy.init(args=args)
    node = GetParams()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
